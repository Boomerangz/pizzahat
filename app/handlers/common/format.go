package common

import (
	"encoding/json"
	"net/http"

	"github.com/kataras/iris"
)

type FormattedResponse struct {
	Response interface{} `json:"response"`
}

type FormattedError struct {
	Error interface{} `json:"error"`
	Code  int         `json:"error_code"`
}

func FormatResponse(response interface{}, code int, ctx iris.Context) {
	formattedResponse := FormattedResponse{
		Response: response,
	}
	serializedResponse, err := json.Marshal(formattedResponse)
	if err != nil {
		FormatError(err, http.StatusInternalServerError, ctx)
		return
	}
	ctx.ContentType("application/json")
	ctx.StatusCode(code)
	ctx.Write(serializedResponse)
}

func FormatError(err error, code int, ctx iris.Context) {
	formattedError := FormattedError{
		Error: err.Error(),
		Code:  code,
	}
	serializedResponse, err := json.Marshal(formattedError)
	if err != nil {
		panic(err)
	}
	ctx.ContentType("application/json")
	ctx.StatusCode(code)
	ctx.Write(serializedResponse)
}

func FormatErrorInterface(err interface{}, code int, ctx iris.Context) {
	formattedError := FormattedError{
		Error: err,
		Code:  code,
	}
	serializedResponse, err := json.Marshal(formattedError)
	if err != nil {
		panic(err)
	}
	ctx.ContentType("application/json")
	ctx.StatusCode(code)
	ctx.Write(serializedResponse)
}
