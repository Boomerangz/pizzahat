package ingridient

import (
	"net/http"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
)

func Post(ctx iris.Context) {
	var ingridient models.Ingridient

	rules := govalidator.MapData{
		"title": []string{"required"},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &ingridient,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}

	db := ctx.Values().Get("db").(func() *bongo.Connection)
	err := db().
		Collection(models.COLLECTION_INGRIDIENT).
		Save(&ingridient)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(ingridient, http.StatusCreated, ctx)
}
