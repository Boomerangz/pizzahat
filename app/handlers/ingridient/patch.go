package ingridient

import (
	"errors"
	"net/http"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
	"gopkg.in/mgo.v2/bson"
)

func Patch(ctx iris.Context) {

	db := ctx.Values().Get("db").(func() *bongo.Connection)
	ID := ctx.Params().Get("id")
	var ingridient models.Ingridient
	if !bson.IsObjectIdHex(ID) {
		err := errors.New("Invalid ID")
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	err := db().
		Collection(models.COLLECTION_INGRIDIENT).
		FindById(bson.ObjectIdHex(ID), &ingridient)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	rules := govalidator.MapData{
		"title": []string{},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &ingridient,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}
	err = db().
		Collection(models.COLLECTION_INGRIDIENT).
		Save(&ingridient)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(ingridient, http.StatusOK, ctx)
}
