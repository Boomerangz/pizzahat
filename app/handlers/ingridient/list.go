package ingridient

import (
	"net/http"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"gopkg.in/mgo.v2/bson"
)

func List(ctx iris.Context) {
	db := ctx.Values().Get("db").(func() *bongo.Connection)
	ingridients := []models.Ingridient{}
	singleIngridient := models.Ingridient{}

	results := db().
		Collection(models.COLLECTION_INGRIDIENT).
		Find(bson.M{})

	for results.Next(&singleIngridient) {
		ingridients = append(ingridients, singleIngridient)
	}

	common.FormatResponse(ingridients, http.StatusOK, ctx)
}
