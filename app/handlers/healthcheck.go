package handlers

import (
	"github.com/kataras/iris"
)

func HealthCheck(ctx iris.Context) {
	ctx.Writef("OK")
}
