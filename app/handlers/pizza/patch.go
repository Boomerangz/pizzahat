package pizza

import (
	"errors"
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
)

func Patch(ctx iris.Context) {

	db := ctx.Values().Get("db").(func() *bongo.Connection)
	ID := ctx.Params().Get("id")
	var pizza models.Pizza
	if !bson.IsObjectIdHex(ID) {
		err := errors.New("Invalid ID")
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	err := db().
		Collection(models.COLLECTION_PIZZA).
		FindById(bson.ObjectIdHex(ID), &pizza)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	var pizzaForm PizzaPatchForm

	rules := govalidator.MapData{
		"title": []string{},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &pizzaForm,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}

	if pizzaForm.Title != nil {
		pizza.Title = *pizzaForm.Title
	}

	if pizzaForm.Recipe != nil {
		for _, portionForm := range *pizzaForm.Recipe {
			pizza.Recipe = []models.IngridientPortion{}
			if !bson.IsObjectIdHex(portionForm.IngridientID) {
				common.FormatErrorInterface(fmt.Sprintf("There is not ingridient with ID = %s", portionForm.IngridientID), http.StatusBadRequest, ctx)
				return
			}

			var ingridient models.Ingridient
			err := db().
				Collection(models.COLLECTION_INGRIDIENT).
				FindById(bson.ObjectIdHex(portionForm.IngridientID), &ingridient)
			if err != nil {
				if err.Error() == (bongo.DocumentNotFoundError{}).Error() {
					common.FormatErrorInterface(fmt.Sprintf("There is not ingridient with ID = %s", portionForm.IngridientID), http.StatusBadRequest, ctx)
				} else {
					common.FormatError(err, http.StatusInternalServerError, ctx)
				}
				return
			}
			portion := models.IngridientPortion{
				Ingridient: ingridient,
				Portion:    portionForm.Portion,
				Unit:       portionForm.Unit,
			}
			pizza.Recipe = append(pizza.Recipe, portion)
		}
	}

	err = db().
		Collection(models.COLLECTION_PIZZA).
		Save(&pizza)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(pizza, http.StatusCreated, ctx)
}

type PizzaPatchForm struct {
	Title  *string                  `json:"title"`
	Recipe *[]IngridientPortionForm `json:"recipe"`
}
