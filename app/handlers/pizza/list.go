package pizza

import (
	"net/http"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"gopkg.in/mgo.v2/bson"
)

func List(ctx iris.Context) {
	db := ctx.Values().Get("db").(func() *bongo.Connection)
	pizzas := []models.Pizza{}
	singlePizza := models.Pizza{}

	results := db().
		Collection(models.COLLECTION_PIZZA).
		Find(bson.M{})

	for results.Next(&singlePizza) {
		pizzas = append(pizzas, singlePizza)
	}

	common.FormatResponse(pizzas, http.StatusOK, ctx)
}
