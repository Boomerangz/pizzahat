package pizza

import (
	"errors"
	"net/http"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"gopkg.in/mgo.v2/bson"
)

func Get(ctx iris.Context) {
	db := ctx.Values().Get("db").(func() *bongo.Connection)
	ID := ctx.Params().Get("id")

	var pizza models.Pizza
	if !bson.IsObjectIdHex(ID) {
		err := errors.New("Invalid ID")
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	err := db().
		Collection(models.COLLECTION_PIZZA).
		FindById(bson.ObjectIdHex(ID), &pizza)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(pizza, http.StatusOK, ctx)
}
