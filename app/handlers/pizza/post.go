package pizza

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/Boomerangz/pizzahat/app/handlers/common"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
)

func Post(ctx iris.Context) {
	var pizzaForm PizzaForm

	rules := govalidator.MapData{
		"title": []string{"required"},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &pizzaForm,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}

	db := ctx.Values().Get("db").(func() *bongo.Connection)
	pizza := models.Pizza{
		Title:  pizzaForm.Title,
		Recipe: []models.IngridientPortion{},
	}
	for _, portionForm := range pizzaForm.Recipe {
		if !bson.IsObjectIdHex(portionForm.IngridientID) {
			common.FormatErrorInterface(fmt.Sprintf("There is not ingridient with ID = %s", portionForm.IngridientID), http.StatusBadRequest, ctx)
			return
		}

		var ingridient models.Ingridient
		err := db().
			Collection(models.COLLECTION_INGRIDIENT).
			FindById(bson.ObjectIdHex(portionForm.IngridientID), &ingridient)
		if err != nil {
			if err.Error() == (bongo.DocumentNotFoundError{}).Error() {
				common.FormatErrorInterface(fmt.Sprintf("There is not ingridient with ID = %s", portionForm.IngridientID), http.StatusBadRequest, ctx)
			} else {
				common.FormatError(err, http.StatusInternalServerError, ctx)
			}
			return
		}
		portion := models.IngridientPortion{
			Ingridient: ingridient,
			Portion:    portionForm.Portion,
			Unit:       portionForm.Unit,
		}
		pizza.Recipe = append(pizza.Recipe, portion)
	}

	err := db().
		Collection(models.COLLECTION_PIZZA).
		Save(&pizza)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(pizza, http.StatusCreated, ctx)
}

type PizzaForm struct {
	Title  string                  `json:"title"`
	Recipe []IngridientPortionForm `json:"recipe"`
}

type IngridientPortionForm struct {
	IngridientID string  `json:"ingridient_id"`
	Portion      float64 `json:"portion"`
	Unit         string  `json:"unit"`
}
