package models

import "github.com/go-bongo/bongo"

const COLLECTION_INGRIDIENT = "ingridient"

type Ingridient struct {
	bongo.DocumentBase `bson:",inline"`
	Title              string `json:"title"`
}
