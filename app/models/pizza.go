package models

import "github.com/go-bongo/bongo"

const COLLECTION_PIZZA = "pizza"

type Pizza struct {
	bongo.DocumentBase `bson:",inline"`
	Title              string              `json:"title"`
	Recipe             []IngridientPortion `json:"recipe"`
}

type IngridientPortion struct {
	Ingridient Ingridient `json:"ingridient"`
	Portion    float64    `json:"portion"`
	Unit       string     `json:"unit"`
}
