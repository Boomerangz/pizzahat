package app

import (
	"fmt"

	"github.com/Boomerangz/pizzahat/app/handlers"
	"github.com/Boomerangz/pizzahat/app/handlers/ingridient"
	"github.com/Boomerangz/pizzahat/app/handlers/pizza"
	"github.com/Boomerangz/pizzahat/config"
	"github.com/go-bongo/bongo"
	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
)

type App struct {
	config     *config.Config
	server     *iris.Application
	connection *bongo.Connection
}

func InitApp(config *config.Config) (*App, error) {
	connection, err := initDbConnection(config)
	if err != nil {
		return nil, err
	}

	app := &App{
		config:     config,
		connection: connection,
	}
	app.server = iris.New()

	app.InitRouter()

	return app, nil
}

func (a *App) InitRouter() {
	a.server.UseGlobal(func(ctx iris.Context) {
		ctx.Values().Set("db", a.GetDBConnection)
		ctx.Next() // execute the next handler, in this case the main one.
	})
	if a.config.DebugLog {
		a.server.UseGlobal(logger.New())
	}

	a.server.Get("/healthcheck", handlers.HealthCheck)

	ingridients := a.server.Party("/ingridients")
	ingridients.Get("", ingridient.List)
	ingridients.Post("", ingridient.Post)
	ingridients.Get("/{id:string}", ingridient.Get)
	ingridients.Patch("/{id:string}", ingridient.Patch)

	pizzas := a.server.Party("/pizzas")
	pizzas.Get("", pizza.List)
	pizzas.Post("", pizza.Post)
	pizzas.Get("/{id:string}", pizza.Get)
	pizzas.Patch("/{id:string}", pizza.Patch)

}

func (a *App) Run() {
	// Start the server using a network address.
	a.server.Run(iris.Addr(fmt.Sprintf(":%d", a.config.ServerPort)))
}

func (a *App) Stop() {
	a.server.Shutdown(nil)
}

func (a *App) GetDBConnection() *bongo.Connection {
	return a.connection
}

func initDbConnection(appConfig *config.Config) (*bongo.Connection, error) {
	config := &bongo.Config{
		ConnectionString: appConfig.DBUrl,
		Database:         appConfig.DBName,
	}
	connection, err := bongo.Connect(config)
	return connection, err
}
