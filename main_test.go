package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/google/logger"
	"gopkg.in/mgo.v2/bson"

	"github.com/Boomerangz/pizzahat/app"
	"github.com/Boomerangz/pizzahat/app/models"
	"github.com/Boomerangz/pizzahat/config"
	"github.com/jochasinga/requests"
)

var (
	application *app.App
	testUrl     string = "http://127.0.0.1:8088"
)

func init() {
	config.InitializeConfigFromFile("config_test.json")
	var err error
	application, err = app.InitApp(config.GetConfig())
	time.Sleep(time.Second * 2)
	if err != nil {
		logger.Fatal(err)
	}
	go application.Run()
}

func TestStart(t *testing.T) {
	logger.Info("Started testing")
}

func TestHealthCheck(t *testing.T) {
	res, err := requests.Get(testUrl + "/healthcheck")
	check(err, t)

	if res.StatusCode != 200 {
		t.Errorf("Expected 200 but got %d", res.StatusCode)
	}

	res, err = requests.Get(testUrl + "/not_existing_healthcheck")
	check(err, t)

	if res.StatusCode != 404 {
		t.Errorf("Expected 404 but got %d", res.StatusCode)
	}

}

func TestDatabaseSaveAndGet(t *testing.T) {
	_, err := application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Delete(bson.M{})
	check(err, t)

	ingridient := models.Ingridient{
		Title: "Test",
	}
	err = application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Save(&ingridient)
	check(err, t)

	var anotherIngridient models.Ingridient
	err = application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).FindById(ingridient.Id, &anotherIngridient)
	check(err, t)

	if anotherIngridient.Title != ingridient.Title {
		t.Errorf("Expected %s and got %s", anotherIngridient.Title, ingridient.Title)
	}
}

func TestGetIngridient(t *testing.T) {
	ingridient := models.Ingridient{
		Title: "Test",
	}
	err := application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Save(&ingridient)
	check(err, t)

	res, err := requests.Get(testUrl + "/ingridients/" + ingridient.Id.Hex())
	check(err, t)

	if res.StatusCode != 200 {
		t.Errorf("Expected 200 but got %d", res.StatusCode)
	}

	var response map[string]models.Ingridient
	bytes := res.Bytes()
	err = json.Unmarshal(bytes, &response)
	check(err, t)

	anotherIngridient := response["response"]

	if anotherIngridient.Title != ingridient.Title {
		t.Errorf("Expected '%s' and got '%s'", ingridient.Title, anotherIngridient.Title)
	}

	if anotherIngridient.Id != ingridient.Id {
		t.Errorf("Expected '%v' and got '%v'", ingridient.Title, anotherIngridient.Title)
	}
}

func TestGetListIngridients(t *testing.T) {
	_, err := application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Delete(bson.M{})
	check(err, t)

	COUNT := 20

	for i := 0; i < COUNT; i++ {
		ingridient := models.Ingridient{
			Title: "Test",
		}
		err := application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Save(&ingridient)
		check(err, t)
	}

	res, err := requests.Get(testUrl + "/ingridients/")
	check(err, t)

	if res.StatusCode != 200 {
		t.Errorf("Expected 200 but got %d", res.StatusCode)
	}

	var response map[string][]models.Ingridient
	bytes := res.Bytes()
	err = json.Unmarshal(bytes, &response)
	check(err, t)

	ingridientsList := response["response"]

	if len(ingridientsList) != COUNT {
		t.Errorf("Expected %d ingridients and got %d", COUNT, len(ingridientsList))
	}

	for _, item := range ingridientsList {
		if item.Title != "Test" {
			t.Errorf("Expected item title to be '%s' and got '%s'", "Test", item.Title)
		}
	}
}

func TestPostIvalidIngridient(t *testing.T) {
	payload := map[string]string{
		"not_title": "Invalid Key And Value",
	}
	res, err := requests.PostJSON(testUrl+"/ingridients/", payload)
	check(err, t)

	if res.StatusCode != 400 {
		t.Errorf("Expected 400 but got %d", res.StatusCode)
	}
}

func TestPostIngridient(t *testing.T) {
	payload := map[string]string{
		"title": "Test Title",
	}
	res, err := requests.PostJSON(testUrl+"/ingridients/", payload)
	if err != nil {
		t.Error(err)
		return
	}

	if res.StatusCode != 201 {
		t.Errorf("Expected 201 but got %d", res.StatusCode)
	}

	bytes := res.Bytes()

	var response map[string]models.Ingridient
	err = json.Unmarshal(bytes, &response)
	check(err, t)

	anotherIngridient := response["response"]

	if anotherIngridient.Title != payload["title"] {
		t.Errorf("Expected '%s' and got '%s'", payload["title"], anotherIngridient.Title)
	}
}

func TestPatchIngridient(t *testing.T) {
	ingridient := models.Ingridient{
		Title: "Test Title 1",
	}
	err := application.GetDBConnection().Collection(models.COLLECTION_INGRIDIENT).Save(&ingridient)
	check(err, t)

	payload := map[string]string{
		"title": "Test Title 2",
	}

	bytesPayload, _ := json.Marshal(payload)
	bufferPayload := bytes.NewBuffer(bytesPayload)
	res, err := requests.Patch(fmt.Sprintf("%s/ingridients/%s", testUrl, ingridient.Id.Hex()), "application/json", bufferPayload)
	check(err, t)

	if res.StatusCode != 200 {
		t.Errorf("Expected 200 but got %d", res.StatusCode)
	}

	bytes := res.Bytes()

	var response map[string]models.Ingridient
	err = json.Unmarshal(bytes, &response)
	check(err, t)

	anotherIngridient := response["response"]

	if anotherIngridient.Title != payload["title"] {
		t.Errorf("Expected '%s' and got '%s'", payload["title"], anotherIngridient.Title)
	}
}

func TestFinish(t *testing.T) {
	application.Stop()
}

func check(err error, t *testing.T) {
	if err != nil {
		t.Error(err)
	}
}
