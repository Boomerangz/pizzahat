package main

import (
	"flag"
	"log"

	"github.com/Boomerangz/pizzahat/app"
	"github.com/Boomerangz/pizzahat/config"
)

var (
	configPath *string = flag.String("config", "", "config file path")
)

func init() {
	flag.Parse()

	if configPath != nil && len(*configPath) > 0 {
		config.InitializeConfigFromFile(*configPath)
	}

}

func main() {
	application, err := app.InitApp(config.GetConfig())
	if err != nil {
		log.Fatal(err)
	}

	application.Run()
}
