package config

import (
	"encoding/json"
	"io/ioutil"

	"github.com/google/logger"
)

var config *Config = &Config{
	DBUrl:      "localhost",
	DBName:     "pizzahat",
	ServerPort: 8080,
	DebugLog:   false,
}

type Config struct {
	DBUrl  string `json:"db_url"`
	DBName string `json:"db_name"`

	ServerPort int  `json:"server_port"`
	DebugLog   bool `json:"debug_log"`
}

func InitializeConfigFromFile(configPath string) {
	dat, err := ioutil.ReadFile(configPath)
	if err != nil {
		logger.Fatal(err)
	}

	err = json.Unmarshal(dat, config)
	if err != nil {
		logger.Fatal(err)
	}
}

func GetConfig() *Config {
	return config
}
